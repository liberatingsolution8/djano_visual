import os
from django import template
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.core.files.storage import FileSystemStorage 
from django.shortcuts import redirect
from django.contrib import messages
import pandas as pd


@login_required(login_url="/login/")
def index(request):
    context = {'segment': 'index'}

    if request.method == 'POST':
        uploaded_files = request.FILES['csvfile']

        print(uploaded_files)

    html_template = loader.get_template('home/index.html')
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]

        if load_template == 'admin':
            return HttpResponseRedirect(reverse('admin:index'))
        context['segment'] = load_template
    
        html_template = loader.get_template('home/' + load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('home/page-404.html')
        return HttpResponse(html_template.render(context, request))

    except:
        html_template = loader.get_template('home/page-500.html')
        return HttpResponse(html_template.render(context, request))


@login_required(login_url="/login/")
def csvupload(request):
    context={}

    if request.method == 'POST':
        uploaded_file = request.FILES['csvfile']

       # print(uploaded_file)

        if uploaded_file.name.endswith('.csv'):

            #save the file in media 
            savefile = FileSystemStorage()

            name=savefile.save(uploaded_file.name,uploaded_file)
            d= os.getcwd()
            #print(d)
            file_directory = d+'\media\\'+name

            readfile(file_directory , request)

            return redirect(csvresults)
        
        else:

            messages.warning(request,'File was not uploaded. Please Use CSV file.')


            

        
    html_template = loader.get_template('home/csv.html')
    return HttpResponse(html_template.render(context, request))

def readfile(filename, request):

    global rows,cols,account_no

    my_file = pd.read_csv(filename,engine='python',encoding = 'unicode_escape')    

    data = pd.DataFrame(data=my_file, index=None)

    print(data.columns)
    #account_no = pd.unique(data['Account No'])
    #print(data)
    rows = len(data.axes[0])
    cols = len(data.axes[1])

    #print(rows)
    #print(cols)



def csvresults(request):
    context={
       # 'account_no':account_no
    }

    message = 'Total data found row(s) : '+ str(rows) + ' and colum(s) : '+ str(cols)

    messages.info(request, message)    
    html_template = loader.get_template('home/csv-results.html')
    return HttpResponse(html_template.render(context, request))


    
  